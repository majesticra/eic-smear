//
// Created by romanov on 7/16/2019.
//

#ifndef EJANA_ZEUSDETECTOR_H
#define EJANA_ZEUSDETECTOR_H

#include "eicsmear/smear/Detector.h"

Smear::Detector BuildZeus();


#endif //EJANA_ZEUSDETECTOR_H
